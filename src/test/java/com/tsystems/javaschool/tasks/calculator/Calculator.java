package test.java.com.tsystems.javaschool.tasks;

import java.util.ArrayList;
import java.util.List;

public class Calculator {
    final String digit = "0123456789()+-/*.";

    final String digit1 = "0123456789.";
    final String oper = "+-/*()";
    String st = "";

    public Calculator() {


    }


    public String evaluate(String s) {

        try {


            if (!this.cont(s, digit)) {
                return "null";
            }
            List<String> list = new ArrayList<String>();
            list = addList(s);

            this.opnParentheses(list);
            if (Double.parseDouble(list.get(0)) % 1 == 0) {
                int x1 = (int) Double.parseDouble(list.get(0));
                return Integer.toString(x1);
            } else {
                String x = String.format("%.4f", Double.parseDouble(list.get(0)));

                return x;
            }
        } catch (Exception e) {
            return "null";
        }
    }


    public String opnParentheses(List<String> e) {


        while (e.contains("(") || e.contains(")")) {
            List<String> x = e.subList(e.indexOf("("), e.indexOf(")") + 1);
            int e1 = 0;
            int e2 = 0;
            if (x.subList(x.indexOf("(") + 1, x.indexOf(")") + 1).contains("(")) {
                this.opnParentheses(x.subList(x.indexOf("("), x.indexOf(")") + 1));
            }

            e1 = x.indexOf("(");
            e2 = x.indexOf(")");
            x.remove(e2);
            x.remove(e1);

            this.opnParentheses(x);

        }
        while (e.contains("*") | e.contains("/")) {
            if (e.contains("*")) {
                int i = e.indexOf("*");
                String st = this.op(e.get(i), e.get(i - 1), e.get(i + 1));
                e.remove(i + 1);
                e.remove(i);
                e.remove(i - 1);
                e.add(i - 1, st);

            } else {
                int i = e.indexOf("/");

                String st = this.op(e.get(i), e.get(i - 1), e.get(i + 1));
                e.remove(i + 1);
                e.remove(i);
                e.remove(i - 1);
                e.add(i - 1, st);

            }

        }
        while ((e.contains("+") | e.contains("-"))) {
            if (e.contains("+")) {
                int i = e.indexOf("+");

                String st = this.op(e.get(i), e.get(i - 1), e.get(i + 1));
                e.remove(i + 1);
                e.remove(i);
                e.remove(i - 1);
                e.add(i - 1, st);


            } else {
                int i = e.indexOf("-");

                String st = this.op(e.get(i), e.get(i - 1), e.get(i + 1));
                e.remove(i + 1);
                e.remove(i);
                e.remove(i - 1);
                e.add(i - 1, st);

            }
        }


        while (e.contains("*") | e.contains("/")) {
            if (e.contains("*")) {
                int i = e.indexOf("*");

                String st = this.op(e.get(i), e.get(i - 1), e.get(i + 1));
                e.remove(i + 1);
                e.remove(i);
                e.remove(i - 1);
                e.add(i - 1, st);

            } else {
                int i = e.indexOf("/");

                String st = this.op(e.get(i), e.get(i - 1), e.get(i + 1));
                e.remove(i + 1);
                e.remove(i);
                e.remove(i - 1);
                e.add(i - 1, st);

            }

        }
        while ((e.contains("+") | e.contains("-"))) {
            if (e.contains("+")) {
                int i = e.indexOf("+");

                String st = this.op(e.get(i), e.get(i - 1), e.get(i + 1));
                e.remove(i + 1);
                e.remove(i);
                e.remove(i - 1);
                e.add(i - 1, st);


            } else {
                int i = e.indexOf("-");

                String st = this.op(e.get(i), e.get(i - 1), e.get(i + 1));
                e.remove(i + 1);
                e.remove(i);
                e.remove(i - 1);
                e.add(i - 1, st);

            }

        }
        return st;
    }


    public String op(String o, String s1, String s2) {
        String s3 = "";
        switch (o) {

            case "*":
                s3 = Double.toString(Double.parseDouble(s1) * Double.parseDouble(s2));
                break;

            case "/":
                s3 = Double.toString(Double.parseDouble(s1) / Double.parseDouble(s2));
                break;

            case "+":
                s3 = Double.toString(Double.parseDouble(s1) + Double.parseDouble(s2));
                break;
            case "-":
                s3 = Double.toString(Double.parseDouble(s1) - Double.parseDouble(s2));
                break;

        }

        return s3;

    }


    public boolean cont(String s1, String s2) {
        int count = 0;
        String[] str = s1.split("");
        for (String s : str) {
            if (s2.contains(s)) ;
            else {
                return false;
            }
            if (s.equals(".")) {
                count++;
                if (count >= 2) {
                    return false;
                }
            }

        }
        return true;
    }


    public List<String> addList(String s) {
        List<String> list = new ArrayList<String>();
        String[] str = s.split("");
        for (int i = 0; i < str.length; i++) {
            if (oper.contains(str[i])) {
                list.add(str[i]);
            } else if (digit1.contains(str[i])) {
                st = str[i];
                while ((i + 1) < str.length && digit1.contains(str[i + 1])) {
                    i++;
                    st = st + str[i];

                }
                list.add(st);

            }

        }

        return list;
    }

}